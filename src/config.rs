use eyre::{
    Result
};
use reqwest::Url;
use serde::Deserialize;
use std::{
    fs::File,
    path::Path,
    path::PathBuf,
};

use crate::koreader;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub xochitl_path: PathBuf,
    pub synchost: String,
    pub syncuser: String,
    pub syncpassword: String,
    pub syncdevice: String,
    pub syncdeviceid: String,
}

impl Config {
    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Config> {
        let f = File::open(path)?;
        Ok(serde_json::from_reader(f)?)
    }

    pub fn syncconf(&self) -> Result<koreader::SyncDconf> {
        Ok(koreader::SyncDconf{
            host: Url::parse(&self.synchost)?,
            user: self.syncuser.clone(),
            password: self.syncpassword.clone(),
            device: self.syncdevice.clone(),
            deviceid: self.syncdeviceid.clone(),
        })
    }
}
