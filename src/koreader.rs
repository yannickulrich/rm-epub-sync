use reqwest::Url;
use eyre::{
    eyre,
    Result
};
use serde::{Deserialize, Serialize};

pub struct SyncDconf {
    pub host: Url,
    pub user: String,
    pub password: String,
    pub device: String,
    pub deviceid: String,
}

#[derive(Default, Serialize, Deserialize, Debug)]
struct ProgressMessage {
    document: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    progress: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    percentage: Option<f32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    device: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    device_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    timestamp: Option<u64>,
}

#[derive(Default, Serialize, Deserialize, Debug)]
struct SyncResponse {
    document: String,
    timestamp: u64,
}


pub fn get(conf: &SyncDconf, hash: String) -> Result<String> {
    let client = reqwest::blocking::Client::new();
    client
        .get(conf.host.join("syncs/progress/")?.join(hash.as_str())?)
        .header("x-auth-user", conf.user.clone())
        .header("x-auth-key", conf.password.clone())
        .send()?
        .json::<ProgressMessage>()?
        .progress
        .ok_or(eyre!("No progress field"))
}

pub fn put(conf: &SyncDconf, hash: String, percentage: f32, xpath: String) -> Result<()> {
    let msg = ProgressMessage {
        document: hash,
        progress: Some(xpath),
        percentage: Some(percentage),
        device: Some(conf.device.clone()),
        device_id: Some(conf.deviceid.clone()),
        timestamp: None,
    };

    let client = reqwest::blocking::Client::new();
    client
        .put(conf.host.join("syncs/progress")?)
        .json(&msg)
        .header("x-auth-user", conf.user.clone())
        .header("x-auth-key", conf.password.clone())
        .send()?
        .json::<SyncResponse>()?;
    Ok(())
}
