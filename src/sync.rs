use eyre::Result;
use crate::{
    config::Config,
    epubindex::EpubIndex,
    heuristic,
    koreader,
    metadata::{RMMetadata, extract_spine},
};

pub fn pull(c: &Config, uuid: String) -> Result<()> {
    let mut m = RMMetadata::from_file(&c.xochitl_path, &uuid)?;
    let index = EpubIndex::from_file(&c.xochitl_path, &uuid)?;
    let spine = extract_spine(&c.xochitl_path, &uuid)?;

    let hash = format!("{:x}", md5::compute(m.visible_name.clone() + ".epub"));

    let xpath = koreader::get(&c.syncconf()?, hash)?;
    m.last_opened_page = heuristic::xpath2page(xpath, &index, &spine)?;

    m.to_file(&c.xochitl_path, &uuid)?;

    Ok(())
}

pub fn push(c: &Config, uuid: String) -> Result<()> {
    let m = RMMetadata::from_file(&c.xochitl_path, &uuid)?;
    let index = EpubIndex::from_file(&c.xochitl_path, &uuid)?;
    let spine = extract_spine(&c.xochitl_path, &uuid)?;

    let hash = format!("{:x}", md5::compute(m.visible_name.clone() + ".epub"));
    let (xpath, percentage) = heuristic::page2xpath(m.last_opened_page, &index, &spine)?;

    koreader::put(&c.syncconf()?, hash, percentage as f32, xpath)?;

    Ok(())
}
