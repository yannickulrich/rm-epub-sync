use eyre::Result;
mod epubindex;
mod heuristic;
mod config;
mod koreader;
mod sync;
mod metadata;

fn main() -> Result<()>{
    let conf = config::Config::from_file("config.json")?;
    sync::pull(&conf, "b35cc6cf-b617-4aac-a4a8-4a43149732af".to_string())?;
    sync::push(&conf, "b35cc6cf-b617-4aac-a4a8-4a43149732af".to_string())?;

    Ok(())

}
