use std::{
    fmt,
    fs::File,
    path::PathBuf,
};
use eyre::{
    eyre,
    Result
};
use binrw::{
    binrw,
    io,
    BinRead,
};

#[binrw]
pub struct SizedUTF16 {
    #[br( temp )]
    #[bw( calc = content.as_bytes().len() as u32 )]
    embedded_size: u32,

    #[br( count = embedded_size/2, try_map = |data: Vec<u16>| String::from_utf16(&data) )]
    #[bw( map = |s| s.as_bytes().to_vec() )]
    content: String
}

impl fmt::Display for SizedUTF16 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.content)
    }
}
impl fmt::Debug for SizedUTF16 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.content)
    }
}

impl From<SizedUTF16> for String {
    fn from(s: SizedUTF16) -> String {
        s.content
    }
}

#[derive(Debug)]
#[binrw]
#[br(big)]
pub struct Chapter {
    name: SizedUTF16,
    file: SizedUTF16,
    page: u32,
    flag: u32,
}

#[derive(Debug)]
#[binrw]
#[br(big)]
pub struct Element {
    pub file: SizedUTF16,
    pub startparagraph: u32,
    pub paragraphs: u32,
    pub startpage: u32,
    pub pages: u32,
}

#[derive(Debug)]
#[binrw]
#[br(big)]
pub struct EpubIndex {
    #[brw(pad_after(32))]
    header: SizedUTF16,
    #[brw(magic = b"\xff\xff\xff\xff")]

    nchapters: u32,
    #[br( count = nchapters )]
    pub chapters: Vec<Chapter>,

    nelements: u32,
    #[br( count = nelements )]
    pub elements: Vec<Element>,
}

impl EpubIndex {
    pub fn from_reader<T: io::Seek + io::Read>(mut reader: T) -> Result<EpubIndex> {
        let ind = EpubIndex::read(&mut reader)?;
        if ind.header.to_string() == "rM epub index" {
            Ok(ind)
        } else {
            Err(eyre!("Bad magic"))
        }
    }

    pub fn from_file(xochitl_path: &PathBuf, uuid: &String) -> Result<Self> {
        let mut path = xochitl_path.clone();
        path.push(uuid.to_owned() + ".epubindex");
        let f = File::open(path)?;
        Ok(Self::from_reader(f)?)
    }
}
