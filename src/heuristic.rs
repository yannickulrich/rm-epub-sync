use eyre::{
    eyre,
    Result,
};
use regex::Regex;

use crate::epubindex::EpubIndex;

pub fn page2xpath(page: u32, index: &EpubIndex, spine: &Vec<String>) -> Result<(String, f64)> {
    let el = index.elements
        .iter()
        .find(|&x| x.startpage + x.pages - 1 > page)
        .ok_or(eyre!("No element found"))?;

    let fragment_id = 1 + spine
        .iter()
        .position(|x| x == &el.file.to_string())
        .ok_or(eyre!("Element not found in spine"))?;

    let ratio: f64 = ((page - el.startpage) as f64) / (el.pages as f64);
    let paragraph = (ratio * (el.paragraphs as f64)) as u32;

    let last = index.elements.last().ok_or(eyre!("Last element not found"))?;
    let npages = last.startpage + last.pages;
    let percentage = (page as f64) / (npages as f64);

    Ok((format!("/body/DocFragment[{}]/body/p[{}]", fragment_id, paragraph), percentage))
}


pub fn xpath2page(xpath: String, index: &EpubIndex, spine: &Vec<String>) -> Result<u32> {
    let re = Regex::new(r"DocFragment\[(\d*)\].*p\[(\d*)\]")?;
    let capt = re.captures(&xpath).ok_or(eyre!("xpath does not match pattern"))?;

    let fragment = capt.get(1).ok_or(eyre!("fragment not found"))?.as_str().parse::<usize>()? - 1;
    let paragraph = capt.get(2).ok_or(eyre!("paragraph not found"))?.as_str().parse::<f64>()?;

    let file = &spine[fragment];
    let el = index.elements
        .iter()
        .find(|&x| &x.file.to_string() == file)
        .ok_or(eyre!("No element found"))?;

    let ratio: f64 = paragraph / (el.paragraphs as f64);
    Ok(1 + el.startpage + (ratio * (el.pages as f64)) as u32)
}
