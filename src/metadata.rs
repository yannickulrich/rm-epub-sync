use epub::doc::EpubDoc;
use eyre::Result;
use serde::{
    Serialize,
    Deserialize,
    Deserializer,
    de,
    de::Unexpected,
};
use std::{
    fs::File,
    fmt,
    path::PathBuf,
};

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct RMMetadata {
    #[serde(deserialize_with = "deserialize_u64")]
    created_time: u64,
    deleted: bool,
    #[serde(deserialize_with = "deserialize_u64")]
    last_modified: u64,
    #[serde(deserialize_with = "deserialize_u64")]
    last_opened: u64,
    pub last_opened_page: u32,
    #[serde(rename = "metadatamodified")]
    metadata_modified: bool,
    modified: bool,
    parent: String,
    pinned: bool,
    synced: bool,
    #[serde(rename = "type")]
    doctype: String,
    version: u64,
    pub visible_name: String,
}

impl RMMetadata {
    pub fn from_file(xochitl_path: &PathBuf, uuid: &String) -> Result<RMMetadata> {
        let mut path = xochitl_path.clone();
        path.push(uuid.to_owned() + ".metadata");
        let f = File::open(path)?;
        Ok(serde_json::from_reader(f)?)
    }

    pub fn to_file(&self, xochitl_path: &PathBuf, uuid: &String) -> Result<()> {
        let mut path = xochitl_path.clone();
        path.push(uuid.to_owned() + ".metadata");
        let f = File::create(path)?;
        serde_json::to_writer_pretty(f, &self)?;
        Ok(())
    }
}

pub fn extract_spine(xochitl_path: &PathBuf, uuid: &String) -> Result<Vec<String>> {
    let mut path = xochitl_path.clone();
    path.push(uuid.to_owned() + ".epub");
    let doc = EpubDoc::new(path)?;
    Ok(doc
        .spine
        .iter()
        .filter_map(
            |s| if let Some((file, mime)) = doc.resources.get(s) {
                if mime == "application/xhtml+xml" {
                    file.clone().into_os_string().into_string().ok()
                } else {
                    None
                }
            } else {
                None
            }
        ).collect::<Vec<_>>())
}

struct DeserializeU64Visitor;

impl<'de> de::Visitor<'de> for DeserializeU64Visitor {
    type Value = u64;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("an integer")
    }

    fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(v)
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        match v.parse::<u64>() {
            Ok(v) => Ok(v),
            Err(_) => Err(E::invalid_value(Unexpected::Str(v), &self))
        }
        // if v == "" {
        //     Ok(0)
        // } else {
        //     Err(E::invalid_value(Unexpected::Str(v), &self))
        // }
    }
}

fn deserialize_u64<'de, D>(deserializer: D) -> Result<u64, D::Error>
where
    D: Deserializer<'de>,
{
    deserializer.deserialize_any(DeserializeU64Visitor)
}
